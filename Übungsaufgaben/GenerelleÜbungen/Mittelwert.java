package Generelle�bungen;
import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte festlegen:
      // ===========================
      double zahl;
      double summe = 0;
      int zaehler = 0;
      char antwort = 'j';
      double mittelwert;
      
      Scanner scan = new Scanner(System.in);
      
      while(antwort == 'j') {
    	  System.out.println("Bitte geben Sie eine Zahl an: ");
    	  zahl = scan.nextDouble();
    	  summe = summe + zahl;
    	  
    	  System.out.print("Haben Sie noch weitere Zahlen? ");
    	  antwort = scan.next().charAt(0);
    	  
    	  zaehler++; // zaehler = zaehler + 1; zaehler +=1/3;
      }
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      mittelwert = summe/zaehler;
      System.out.printf("Mittelwert: %.3f", mittelwert);
   }
}
