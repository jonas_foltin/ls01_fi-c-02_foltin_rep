package Generelle�bungen;
import java.util.Scanner;

/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Jonas Foltin >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    byte anzahlPlaneten =  8;
    
    // Anzahl der Sterne in unserer Milchstra�e (2x0 fehlt)
    long anzahlSterne = 250000000000l;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3762456;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    short alterTage = 19 * 365;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 180000; 
    
    // Schreiben Sie auf, wie viele km� das gr��te Land der Erde hat?
    long flaecheGroessteLand = 17075400;
    
    // Wie gro� ist das kleinste Land der Erde?
    float flaecheKleinsteLand = 0.44f;
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Anzahl der Bewohner von Berlin: " + bewohnerBerlin);
    
    System.out.println("Anzahl der Tage wie alt du bist: " + alterTage);
    
    System.out.println("Anzahl in KG des schwersten Tieres: " + gewichtKilogramm);
    
    System.out.println("Anzahl in km� des gr��ten Landes der Erde: " + flaecheGroessteLand + " km�");
    
    System.out.println("Anzahl in km� des kleinsten Landes der Erde: " + flaecheKleinsteLand + " km�");
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

