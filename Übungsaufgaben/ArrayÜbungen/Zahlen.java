import java.util.Scanner;

public class Zahlen {

	public static void main(String[] args) {
		// Aufgabe 1: Zahlen 0-9 ausgeben
		System.out.println("Aufgabe 1:");
		int[] zahlen = new int[10];
		
		for(int i = 0; i < zahlen.length; i++) {
			zahlen[i] = i;
			
			System.out.print(zahlen[i] + " ");
		}
		
		System.out.println("\n");
		
		// Aufgabe 2: ungerade Zahlen
		System.out.println("Aufgabe 2:");
		int[] UngeradeZahlen = new int[10];
		
		int arrayValue = 0;
		for(int j = 0; j <= 19; j++) {	
			if(j % 2 != 0) {
				UngeradeZahlen[arrayValue] = j;
				System.out.print(UngeradeZahlen[arrayValue] + " ");
				arrayValue++;
			}
		}
		
		System.out.println("\n");
		
		// Aufgabe 3: Palindrom
		System.out.println("Aufgabe 3:");
		Scanner tastatur = new Scanner(System.in);
		char[] Zeichen = new char[5];
		
		String eingabe = tastatur.next();
		Zeichen = eingabe.toCharArray();
		
		//System.out.print(Zeichen.length);
		
		int i = Zeichen.length;
		while(i != 0) {
			i = i - 1;
			System.out.print(Zeichen[i]);
		}

	}

}
