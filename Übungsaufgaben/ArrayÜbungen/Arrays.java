
public class Arrays {

	public static void main(String[] args) {
		
		int[] zlist;
		zlist = new int[5];
		
		// int[] zlist = new int[10];

		
		zlist[0] = 10;
		zlist[1] = 11;
		zlist[2] = 12;
		zlist[3] = 13;
		zlist[4] = 14;
		
		for(int i = 0; i < 100; i++) {
			zlist[i] = i + 10;
		}
		
		for(int i = 0; i  < zlist.length; i++) {
			System.out.println(zlist[i]);
		}
		
	}

}
