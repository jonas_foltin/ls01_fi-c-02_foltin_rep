package SchleifenBeispiele;
import java.util.Scanner;

public class SchleifenAufgabe1 {

	public static void main(String[] args) {
		// Die Zahl n einlesen
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben sie n ein: ");
		int n = myScanner.nextInt();
		
		// wird 1+2+3+...+n
		int z�hler = 1;
		int erg = 0;
		while (z�hler <= n) {
			erg = erg + z�hler;
			z�hler++;
		}
		
		// Ergebnis ausgeben
		System.out.println("Ergebnis: " + erg);
	}

}
