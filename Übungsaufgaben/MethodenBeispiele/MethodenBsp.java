package MethodenBeispiele;

public class MethodenBsp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int zahl1 = 5;
		int zahl2 = 7;
		int erg1 = min(zahl1, zahl2);
		
		int erg2 = max(3,6,6);
		
		System.out.println("1. Ergebnis: " + erg1);
		System.out.println("2. Ergebnis: " + erg2);
	}
	
	public static int min(int zahl1, int zahl2) {
		if(zahl1 < zahl2) {
			return zahl1;
		} else {
			return zahl2;
		}
	}
	
	public static int max(int a, int b, int c) {
		if (a > b && a > c) {
			return a;
		} else if (b > a && b > c) {
			return b;
		} else {
			return c;
		}
		
	}

}
