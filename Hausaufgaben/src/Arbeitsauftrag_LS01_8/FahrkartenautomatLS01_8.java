﻿package Arbeitsauftrag_LS01_8;
import java.util.Scanner;
import java.lang.Math;

class FahrkartenautomatLS01_8
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
       // Fahrkartenautomat von Jonas Foltin - FI-C 02, 09.09.2020
       // Achtung!!!!
       // Hier wird deutlich, warum double nicht für Geldbeträge geeignet ist.
       // =======================================
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       double gesamtpreisTickets;
       // Die Eingabe erfolgt anwenderfreudlich mit Dezimalpunkt: just testing
       double eingegebenerBetrag;
       int anzahlTickets;
       
       // Den zu zahlenden Betrag ermittelt normalerweise der Automat
       // aufgrund der gewählten Fahrkarte(n).
       // -----------------------------------
       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();

       System.out.print("Anzahl der Tickets: ");
       anzahlTickets = tastatur.nextInt();
       
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.00;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag * anzahlTickets)
       {
    	   System.out.printf("%s %.2f %s %n", "Noch zu zahlen:", (zuZahlenderBetrag * anzahlTickets - eingezahlterGesamtbetrag), "€" );
    	   System.out.print("Eingabe (mind. 5 Cent, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       gesamtpreisTickets = zuZahlenderBetrag * anzahlTickets;
       rückgabebetrag = eingezahlterGesamtbetrag - gesamtpreisTickets;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.printf("%s %.2f %s %n", "Der Rückgabebetrag in Höhe von:", rückgabebetrag, "€");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.00) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.00;
           }
           while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.00;
           }
           while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.50;
           }
           while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.20;
           }
           while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.10;
           }
           while(rückgabebetrag >= 0.05) // 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
           while(rückgabebetrag >= 0.02) // 2 CENT-Münzen
           {
        	  System.out.println("2 CENT");
 	          rückgabebetrag -= 0.02;
           }
           while(rückgabebetrag >= 0.01) // 1 CENT-Münzen
           {
        	  System.out.println("1 CENT");
 	          rückgabebetrag -= 0.01;
           }
           while(rückgabebetrag >= 0.001) // 1 CENT-Münzen
           {
        	  System.out.println("1 CENT");
 	          rückgabebetrag -= 0.01;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}