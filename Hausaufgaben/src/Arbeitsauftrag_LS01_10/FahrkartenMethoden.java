package Arbeitsauftrag_LS01_10;
import java.util.Scanner;

class FahrkartenMethoden
{
	
    public static void main(String[] args) {
        Scanner tastatur = new Scanner(System.in);
        // Achtung!!!!
        // Hier wird deutlich, warum double nicht f�r Geldbetr�ge geeignet ist.
        // =======================================
        double zuZahlenderBetrag;
        int anzahlTickets;
        double r�ckgabebetrag;

        // Den zu zahlenden Betrag ermittelt normalerweise der Automat
        // aufgrund der gew�hlten Fahrkarte(n).
        // -----------------------------------
        zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);

        // Fahrkartenanzahl
        // ----------------
        anzahlTickets = fahrkartenAnzahl(tastatur);
        
        // Geldeinwurf
        // -----------
        r�ckgabebetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag, anzahlTickets);

        // Fahrscheinausgabe
        // -----------------
        fahrkartenAusgeben();

        // R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        rueckgeldAusgeben(r�ckgabebetrag);

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir w�nschen Ihnen eine gute Fahrt.");
    }
    
    public static double fahrkartenbestellungErfassen(Scanner tastatur){
        double zuZahlenderBetrag;

        System.out.print("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble();
        return zuZahlenderBetrag;
    }
    
    public static int fahrkartenAnzahl(Scanner tastatur) {
    	int anzahlTickets;
    	
    	System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();
        return anzahlTickets;
    }
    
    private static double fahrkartenBezahlen(Scanner tastatur, double zuZahlenderBetrag, int anzahlTickets) {
    	double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneM�nze;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag * anzahlTickets)
        {
        	System.out.printf("%s %.2f %s %n", "Noch zu zahlen:", (zuZahlenderBetrag * anzahlTickets - eingezahlterGesamtbetrag), "�" );
            System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
            eingeworfeneM�nze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag * anzahlTickets;
    }
    
    private static void rueckgeldAusgeben(double r�ckgabebetrag) {
        if(r�ckgabebetrag > 0.0)
        {
        	System.out.printf("%s %.2f %s %n", "Der R�ckgabebetrag in H�he von:", r�ckgabebetrag, "�");
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
                System.out.println("2 EURO");
                r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
                System.out.println("1 EURO");
                r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
                System.out.println("50 CENT");
                r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
                System.out.println("20 CENT");
                r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
                System.out.println("10 CENT");
                r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
                System.out.println("5 CENT");
                r�ckgabebetrag -= 0.05;
            }
            while(r�ckgabebetrag >= 0.02) // 2 CENT-M�nzen
            {
         	  System.out.println("2 CENT");
  	          r�ckgabebetrag -= 0.02;
            }
            while(r�ckgabebetrag >= 0.01) // 1 CENT-M�nzen
            {
         	  System.out.println("1 CENT");
  	          r�ckgabebetrag -= 0.01;
            }
            while(r�ckgabebetrag >= 0.001) // 1 CENT-M�nzen
            {
         	  System.out.println("1 CENT");
  	          r�ckgabebetrag -= 0.01;
            }
        }
    }

    private static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }
}